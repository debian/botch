
open OUnit


let test_sources =
  "test_sources" >::: [
    "test 1" >:: (fun _ -> ());
  ]
;;

let all =
  "all tests" >::: [
    test_sources
  ]

let main () =
  OUnit.run_test_tt_main all
;;

main ()

