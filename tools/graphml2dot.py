#!/usr/bin/env python3

import networkx as nx
import sys

sys.path.append("/usr/share/botch")
from util import read_graphml, write_dot


def string_of_bin(name, arch, version):
    return "%s:%s (= %s)" % (name, arch, version)


def string_of_src(name, version):
    return "src:%s (= %s)" % (name, version)


def graphml2dot(g):
    h = nx.DiGraph()

    for n, d in g.nodes(data=True):
        t = g.nodes[n].get("type")
        if t in ["src", None]:
            h.add_node(
                n, label=string_of_src(d["name"], d["version"]), shape="rectangle"
            )
        else:
            h.add_node(
                n,
                label=string_of_bin(d["name"], d["architecture"], d["version"]),
                shape="ellipse",
            )

    for efrom, eto, edata in g.edges(data=True):
        k = edata.get("kind")
        if k in ["builddep", None]:
            h.add_edge(efrom, eto, style="dashed")
        else:
            h.add_edge(efrom, eto)

    return h


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Convert a graph in GraphML format to dot format"
    )
    parser.add_argument(
        "g",
        type=read_graphml,
        nargs="?",
        default="-",
        help="Input graph in GraphML format (default: stdin)",
    )
    parser.add_argument(
        "h",
        type=write_dot,
        nargs="?",
        default="-",
        help="Output graph in dot format (default: stdout)",
    )
    parser.add_argument("-v", "--verbose", action="store_true", help="be verbose")
    args = parser.parse_args()
    h = graphml2dot(args.g)
    args.h(h)
