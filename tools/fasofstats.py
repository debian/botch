#!/usr/bin/env python3

from __future__ import print_function
import sys

sys.path.append("/usr/share/botch")
from util import read_json_file


# string of package
def sos(srcpkg):
    name, ver = srcpkg
    return "src:" + name


def sob(binpkg):
    name, arch, ver = binpkg
    return name


# string of list
def sobl(L):
    return " ".join([sob(pkg) for pkg in L])


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description=("extract feedback arc set from stats.json")
    )
    parser.add_argument(
        "stats", metavar="stats.json", type=read_json_file, help="input json"
    )
    parser.add_argument("-v", "--verbose", action="store_true", help="be verbose")
    args = parser.parse_args()
    for scc in args.stats["buildgraph"]["sccs"]:
        for src, deps in scc["fas"]:
            print(sos(src), sobl(deps))
