#!/usr/bin/python3

import networkx as nx
import sys

sys.path.append("/usr/share/botch")
from util import read_graph, write_graph, find_node


def descendants(g, source):
    source = find_node(g, source)
    desc = nx.algorithms.descendants(g, source)
    desc.add(source)
    h = g.subgraph(desc)
    h.input_file_type = g.input_file_type
    return h


def vertex(string):
    try:
        key, value = string.split(":", 1)
    except ValueError:
        raise argparse.ArgumentTypeError("key must be separated from value by a colon")
    return key, value


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="find all descendants of a source vertex in a graph in "
        "GraphML or dot format"
    )
    parser.add_argument(
        "g",
        type=read_graph,
        nargs="?",
        default="-",
        help="Input graph in GraphML or dot format (default: " "stdin)",
    )
    parser.add_argument(
        "h",
        type=write_graph,
        nargs="?",
        default="-",
        help="Output graph in GraphML or dot format " "(default: stdout)",
    )
    parser.add_argument(
        "--source",
        required=True,
        type=vertex,
        action="append",
        help="key:value pairs to match the source vertex. "
        "The special key __ID__ allows one to select "
        "the unique vertex identifier.",
    )
    parser.add_argument("-v", "--verbose", action="store_true", help="be verbose")
    args = parser.parse_args()
    h = descendants(args.g, args.source)
    args.h(h)
