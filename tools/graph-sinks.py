#!/usr/bin/python3

import networkx as nx
import sys

sys.path.append("/usr/share/botch")
from util import read_graph, write_graph


def sinks(g):
    sinks = [n for n in nx.nodes(g) if len(g.successors(n)) == 0]
    h = g.subgraph(sinks)
    h.input_file_type = g.input_file_type
    return h


def vertex(string):
    try:
        key, value = string.split(":", 1)
    except ValueError:
        raise argparse.ArgumentTypeError("key must be separated from value by a colon")
    return key, value


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="find all sinks (vertices without successors) in a graph "
        "in GraphML or dot format"
    )
    parser.add_argument(
        "g",
        type=read_graph,
        nargs="?",
        default="-",
        help="Input graph in GraphML or dot format (default: " "stdin)",
    )
    parser.add_argument(
        "h",
        type=write_graph,
        nargs="?",
        default="-",
        help="Output graph in GraphML format or dot format" "(default: stdout)",
    )
    parser.add_argument("-v", "--verbose", action="store_true", help="be verbose")
    args = parser.parse_args()
    h = sinks(args.g)
    args.h(h)
