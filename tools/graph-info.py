#!/usr/bin/env python3

from __future__ import print_function
import networkx as nx
import sys

sys.path.append("/usr/share/botch")
from util import read_graph

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description=(
            "Print type and number of vertices, edges and SCC of a "
            + "graph in GraphML or dot format"
        )
    )
    parser.add_argument(
        "g", type=read_graph, help="Input graph in GraphML or dot format"
    )
    parser.add_argument("-v", "--verbose", action="store_true", help="be verbose")
    args = parser.parse_args()

    if "InstSet" in (a["kind"] for _, a in args.g.nodes(data=True)):
        graphtype = "buildgraph"
    else:
        graphtype = "srcgraph"

    nvert = args.g.number_of_nodes()
    nedge = args.g.number_of_edges()
    nscc = len(
        [
            1
            for scc in nx.components.strongly_connected_components(args.g)
            if len(scc) > 1
        ]
    )

    print("%s\t%d\t%d\t%d" % (graphtype, nvert, nedge, nscc))
