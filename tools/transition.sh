#!/bin/sh -ex

if [ $1 = "--help" ]; then
	echo "usage: $0 [query [date]]"
	exit 0
fi

defaultdate=20130101T032636Z
date=${2:-defaultdate}
NATIVEARCH=amd64
defaultquery=".maintainer ~ /debian-ocaml-maint/ | .build-depends ~ /ocaml/ | .build-depends-indep ~ /ocaml/ | .depends ~ /ocaml(-base)?(-nox)?-3\.1(1|2)\../"
query=${1:-defaultquery}
sources=Sources
packages=Packages_${NATIVEARCH}

# create ben.cache

ben download --mirror http://snapshot.debian.org/archive/debian/${date}/ --use-cache --archs $NATIVEARCH

# find all relevant binary packages

ben query "( $query ) & ! source" ben.cache > ocaml-bin

# and get their source packages including those for Architecture:all binary packages

./bin2src.native --deb-native-arch=$NATIVEARCH --allowsrcmismatch ocaml-bin "$sources" > ocaml-src1

# find all relevant source packages

ben query "( $query ) & source" ben.cache > ocaml-src2

# calculate the union of both source package sets

./tools/packages-union.py ocaml-src1 ocaml-src2 - > ocaml-src

# only select the latest package versions

./tools/latest-version.py ocaml-src - > ocaml-src-latest

# find the binary packages they build

./src2bin.native --deb-native-arch=$NATIVEARCH --allowsrcmismatch --bg "$sources" "$packages" ocaml-src-latest > ocaml-bin

# the available binary packages are all except the ocaml packages

./tools/packages-difference.py "$packages" ocaml-bin - > available

# when creating the graph it is important to drop Build-Depends-Indep

./create_graph.native -v --progress --timers --deb-native-arch=$NATIVEARCH -A available "$packages" ocaml-src-latest

./create_build_order.native -v --deb-native-arch=$NATIVEARCH
