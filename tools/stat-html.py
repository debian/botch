#!/usr/bin/env python3

from __future__ import print_function
from datetime import datetime
import sys

sys.path.append("/usr/share/botch")
from util import read_json_file

from functools import reduce


def partition(L, p):
    return reduce(lambda x, y: x[not p(y)].append(y) or x, L, ([], []))


# string of package
def sob(binpkg):
    name, arch, ver = binpkg
    return '<span class="pkg" title="%s:%s&nbsp;(=&nbsp;%s)">%s</span>' % (
        name,
        arch,
        ver,
        name,
    )


def sos(srcpkg):
    name, ver = srcpkg
    return '<a href="/source/%s_%s.html" class="pkg"' % (
        name,
        ver,
    ) + ' title="src:%s&nbsp;(=&nbsp;%s)">src:%s</a>' % (name, ver, name)


# string of list
def sobl(L):
    return ", ".join([sob(pkg) for pkg in L])


def sosl(L):
    return ", ".join([sos(pkg) for pkg in L])


# string of vector
def sov(vertex):
    t, data = vertex
    if t == "SrcPkg":
        name, ver = data
        fullname = "src:%s&nbsp;(=&nbsp;%s)" % (name, ver)
        return '<a href="/source/%s_%s.html" class="pkg"' % (
            name,
            ver,
        ) + ' title="%s">src:%s</a>' % (fullname, name)
    elif t == "InstSet":
        (name, arch, ver), instset = data
        instset = ", ".join(
            ["%s:%s&nbsp;(=&nbsp;%s)" % (n, a, v) for n, a, v in instset]
        )
        fullname = "%s:%s&nbsp;(=&nbsp;%s), installation set = { %s }" % (
            name,
            arch,
            ver,
            instset,
        )
        return '<span class="pkg" title="%s">%s</span>' % (fullname, name)
    elif t == "SCC":
        raise Exception("SCC not implemented yet")
    else:
        raise Exception("invalid vertex type: %s" % t)


# string of cycle


def soc(c):
    res = sov(c[0])
    for v in c[1:]:
        t, data = v
        if t == "SrcPkg":
            # the previous edge was a "buildsfrom" edge
            res += (
                ' <span title="builds from" class="arrowouter">'
                + '<span class="arrowinner">→</span></span> '
            )
        elif t == "InstSet":
            # the previous edge was a "builddepends" edge
            res += (
                ' <span title="build depends" class="arrowouter">'
                + '<span class="arrowinner">⇢</span></span> '
            )
        else:
            raise Exception("invalid vertex type: %s" % t)
        res += sov(v)
    return res


tables = {
    "srcgraph": {
        "type1cycles": {
            "title": "Type 1 Self-Cycles",
            "descr": "<p>A source package directly Build-Depends on a "
            + "binary package it itself builds</p>",
            "heads": [
                "source package",
                "strongly depends on",
                "because of the source package build depending on",
            ],
            "printer": lambda src, l1, l2: (sos(src), sobl(l1), sobl(l2)),
            "order": 0,
        },
        "type2cycles": {
            "title": "Type 2 Self-Cycles",
            "descr": "<p>A source indirectly strongly Build-Depends on a "
            + "binary package it itself builds through one of its "
            + "direct build dependencies.</p>",
            "heads": [
                "source package",
                "strongly depends on",
                "because of the source package build depending on",
            ],
            "printer": lambda src, l1, l2: (sos(src), sobl(l1), sobl(l2)),
            "order": 0,
        },
        "type3cycles": {
            "title": "Type 3 Self-Cycles",
            "descr": "<p>Not-strong self-cycles which can probably be "
            + "solved by choosing a different installationset</p>",
            "heads": ["source package", "in non-strong cycle with"],
            "printer": lambda src, deps: (sos(src), sobl(deps)),
            "order": 0,
        },
    },
    "buildgraph": {
        "cycles": {
            "title": "Cycles",
            "descr": "Small dependency cycles. The second column shows the "
            + "packages involved in the cycle.",
            "heads": ["Length", "Cycle"],
            "printer": lambda *row: ("%d" % (len(row)), soc(row)),
            "order": 0,
        },
        "cycleedge": {
            "title": "Amount of Cycles through Edges",
            "descr": "After enumerating all cycles in the graph up to a "
            + "certain maximum length, the first column indicates "
            + "the amount of cycles that the edge in the second "
            + "column is part of. Removing an edge with many cycles "
            + "through it, immediately breaks all those cycles.",
            "heads": ["Amount", "Edge"],
            "printer": lambda edge, a: ("%d" % (a), soc(edge)),
            "order": 1,
        },
        "builddeps": {
            "title": "Amount of Missing Build Dependencies",
            "descr": "Source packages which only lack very few build dependencies.",
            "heads": ["Amount", "Source Package", "Build Dependencies"],
            "printer": lambda src, deps: ("%d" % (len(deps)), sos(src), sobl(deps)),
            "order": 0,
        },
        "ratio_source": {
            "title": "Ratio Source Heuristic",
            "descr": """
<p>Find source packages that build-depend on many others but are only needed by
few binary packages which are in turn only needed buy a few source packages.
Maybe the source packages that need those few binary packages can be built
without them?</p>

<p>In other words: The source package in the second column draws in many binary
packages as build dependencies (amount in the third column). But that source
package must only be built because of few binary packages (fourth column) which
in turn are only needed by few other source packages as build dependencies
(fifth column). If the source packages in the fifth column could be built
without the binary packages in the fourth column, then the source package of
the second column would not be needed anymore and would therefor not draw in
the amount of binary packages in the third column.</p>

<p>The value of the first column is the result of dividing the value in the
third column by the amount of packages in the fifth column</p>""",
            "heads": [
                "Ratio",
                "Source Package",
                "Missing Build Dependencies",
                "Source Package is needed by",
                "Those Packages are Build Dependencies of",
            ],
            "printer": lambda s, b, n, o: (
                "%.02f" % (float(b) / len(o)),
                sos(s),
                "%d" % (b),
                sobl(n),
                sosl(o),
            ),
            "order": 1,
        },
        "ratio_binary": {
            "title": "Ratio Binary Heuristic",
            "descr": """
<p>Find binary packages that are only needed by few source package but need
many other source packages to be built to satisfy their runtime dependencies.
Maybe the source package that needs this binary package can be built without
it?</p>

<p>In other words: The binary package in the second column draws in many source
packages (amount in the third column) because of its installation set. If the
source package in the fourth column could be built without the binary package
in the second column, then the binary package in the second column would not be
needed anymore and would therefor not draw in the amount of source packages in
the third column.</p>

<p>The value of the first column is the result of dividing the value in the
third column by the amount of packages in the fourth column</p>""",
            "heads": [
                "Ratio",
                "Binary Package",
                "Needs Source Package",
                "Is Build Dependency of",
            ],
            "printer": lambda b, s, o: (
                "%.02f" % (float(s) / len(o)),
                sob(b),
                "%d" % (s),
                sosl(o),
            ),
            "order": 1,
        },
        "weak": {
            "title": "Only Weak Build Dependencies Missing",
            "descr": '<p>Source packages that only lack "weak" '
            + "build dependencies as classified as such by "
            + '<a href="https://salsa.debian.org/debian-bootstrap-team/'
            + 'botch/-/raw/master/droppable/weak-build-dependencies.list"'
            + ">this list</a>.</p>",
            "heads": ["Amount", "Source Package", "Weak Build Dependencies"],
            "printer": lambda src, deps: ("%d" % (len(deps)), sos(src), sobl(deps)),
            "order": 0,
        },
        "srcbinstats": {
            "title": "Vertex Statistics",
            "descr": "<p>Several vertex degree based statistics. The first "
            + "column shows the source vertex or installation set "
            + "vertex name. The second column shows the amount of "
            + "ingoing edges and the third column the amount of "
            + "outgoing edges. The fourth column shows the ratio and "
            + "the fifth column the sum of those two.<p>",
            "heads": [
                "Source package or Installation Set",
                "In-Degree",
                "Out-Degree",
                "Ratio",
                "Connectedness",
            ],
            "printer": lambda v, outd, ind: (
                sov(v),
                "%d" % (ind),
                "%d" % (outd),
                "%.03f" % (float(ind) / float(outd)),
                "%d" % (ind + outd),
            ),
            "order": 0,
        },
        "fas": {
            "title": "Feedback Arc Set",
            "descr": "A feedback arc set which would make the dependency "
            + "graph acyclic.",
            "heads": ["Source Package", "Build Dependencies to Drop"],
            "printer": lambda src, deps: (sos(src), sobl(deps)),
            "order": 0,
        },
        "sap": {
            "title": "Strong Articulation Points",
            "descr": "If the vertex shown in the second column could be "
            + "removed from the dependency graph, then this strongly "
            + "connected component would be broken into as many "
            + "strongly connected components as shown in the first "
            + "column.",
            "heads": ["Amount of SCC", "Vertex"],
            "printer": lambda vert, a: ("%d" % (a), sov(vert)),
            "order": 1,
        },
        "sb": {
            "title": "Strong Bridges",
            "descr": "If the edge shown in the second column could be "
            + "removed from the dependency graph, then this strongly "
            + "connected component would be broken into as many "
            + "strongly connected components as shown in the first "
            + "column.",
            "heads": ["Amount of SCC", "Edge"],
            "printer": lambda edge, a: ("%d" % (a), soc(edge)),
            "order": 1,
        },
    },
}

controls = """<form>
  <a href="#" class="first">first</a>
  <a href="#" class="prev">prev</a>
  <input type="text" class="pagedisplay"/>
  <a href="#" class="next">next</a>
  <a href="#" class="last">last</a>
  <select class="pagesize">
    <option selected="selected" value="10">10</option>
    <option value="25">25</option>
    <option value="50">50</option>
    <option value="100">100</option>
  </select>
  per page (%d rows in total)
</form>"""


def render_section_srcgraph(name, dat):
    table = tables["srcgraph"][name]
    print('<div id="%s">' % name)
    print('<a name="%s"></a>' % name)
    print("<h2>%s</h2>" % (table["title"]))
    print(table["descr"])
    render_table(table, name, dat, 0)
    print("</div>")


def render_section_buildgraph(name, sccs):
    table = tables["buildgraph"][name]
    print('<div id="%s">' % name)
    print('<a name="%s"></a>' % name)
    print("<h2>%s</h2>" % (table["title"]))
    print(table["descr"])
    for i, dat in sccs:
        render_table(table, name, dat, i + 1)
    print("</div>")


def render_table(table, name, dat, num):
    print('<div class="scc_%d">' % num)
    if num != 0:
        print("<h3>SCC #%d</h3>" % num)
    print('<div id="%s_%d_pager">%s</div>' % (name, num, controls % len(dat[name])))
    print('<table class="tablesorter %s" id="%s_%d">' % (name, name, num))
    print("<thead><tr>")
    for head in table["heads"]:
        print("<th>%s</th>" % head)
    print("</tr></thead>\n<tbody>")
    if dat[name]:
        for row in dat[name]:
            # we have to do this so that it works in python2 and python3
            # see http://stackoverflow.com/questions/23944976
            sys.stdout.flush()
            getattr(sys.stdout, "buffer", sys.stdout).flush()
            getattr(sys.stdout, "buffer", sys.stdout).write(
                (
                    "<tr><td>"
                    + "</td><td>".join(table["printer"](*row))
                    + "</td></tr>\n"
                ).encode("utf8")
            )
            getattr(sys.stdout, "buffer", sys.stdout).flush()
            sys.stdout.flush()
    else:
        print('<tr><td colspan="%d">empty</td></tr>\n' % len(table["heads"]))
    print("</tbody></table>")
    print("</div>")


def stat_html(data, online=False, verbose=False):
    # sort SCCs with descending number of vertices
    # if the number of vertices is the same, sort by the number of edges, then
    # by the vertex properties
    sccs = list(
        enumerate(
            sorted(
                data["buildgraph"]["sccs"],
                key=lambda x: (x["sg_vertex"], x["sg_edges"], x["srcbinstats"]),
                reverse=True,
            )
        )
    )

    if online:
        webresources_style = "/webresources"
        webresources_jquery = "/webresources"
        webresources_tablesorter = "/webresources"
        webresources_pager = "/webresources"
    else:
        webresources_style = "file:///usr/share/javascript/jquery-tablesorter/css"
        webresources_jquery = "file:///usr/share/javascript/jquery"
        webresources_tablesorter = "file:///usr/share/javascript/jquery-tablesorter"
        webresources_pager = (
            "file:///usr/share/javascript/jquery-tablesorter/addons/pager"
        )

    print(
        """<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title>Debian Dependency Graph Analysis Results</title>
    <link href="%s/theme.blue.css" rel="stylesheet" />
    <style>
    body {margin-top:6em}
    table {font-size: 1em !important}
    .pkg:hover {background-color: #e6EEEE}
    #topbar {
        position:fixed;
        top:0px;
        left:0px;
    }
    ul {list-style-type:none;padding-left:0.5em}
    .topbutton {border:2px solid #CDCDCD;
        padding:0em 0.5em;float:left;margin:0.5em;
        background-color:#FFF}
    .topbutton ul {height:0px;overflow:hidden;margin:0px;}
    .topbutton:hover ul {height:auto;margin-bottom:0.5em}
    .topbutton h1 {font-family:arial;font-size: 1.2em;}
    .arrowouter {
        display: inline-block;
        height: 1em;
        overflow: hidden;
        vertical-align: bottom;
    }
    .arrowinner {
        display:block;
        font-size: 2em;
        margin-top: -0.4em;
    }
    </style>
    <script type="text/javascript" src="%s/jquery.js"></script>
    <script type="text/javascript" src="%s/jquery.tablesorter.js"></script>
    <script type="text/javascript" src="%s/jquery.tablesorter.pager.js">"""
        % (
            webresources_style,
            webresources_jquery,
            webresources_tablesorter,
            webresources_pager,
        )
        + """</script>


    <script>
      function toggle_hidesrc () {
        if ($('#hidesrc').is(':checked')) {
          $("#srcgraph").hide();
        } else {
          $("#srcgraph").show();
        }
      }
      function toggle_hidebuild () {
        if ($('#hidebuild').is(':checked')) {
          $("#buildgraph").hide();
        } else {
          $("#buildgraph").show();
        }
      }
      function toggle_scc (i) {
        if ($('#show_scc_'+i).is(':checked')) {
          $('.scc_'+i).show();
        } else {
          $('.scc_'+i).hide();
        }
      }
      function toggle_result (i) {
        if ($('#show_'+i).is(':checked')) {
          $('#'+i).show();
          $('#'+i+'_toc').show();
        } else {
          $('#'+i).hide();
          $('#'+i+'_toc').hide();
        }
      }
      function toggle_scc_all () {"""
    )
    for i, scc in sccs:
        print("        toggle_scc(%d)" % (i + 1))
    print(
        """      }
      function toggle_result_all () {"""
    )
    for i in sorted(list(tables["srcgraph"].keys())) + sorted(
        list(tables["buildgraph"].keys())
    ):
        print("        toggle_result('%s')" % i)
    print(
        """      }
      function hide_scc_all () {
        $.each($(".show_scc"), function(index, val) {
          $(val).prop('checked', true);
        });
        toggle_scc_all();
      }
      function show_scc_all () {
        $.each($(".show_scc"), function(index, val) {
          $(val).prop('checked', false);
        });
        toggle_scc_all();
      }
      function hide_srcresult_all () {
        $.each($(".show_srcresult"), function(index, val) {
          $(val).prop('checked', false);
        });
        toggle_result_all();
      }
      function show_srcresult_all () {
        $.each($(".show_srcresult"), function(index, val) {
          $(val).prop('checked', true);
        });
        toggle_result_all();
      }
      function hide_buildresult_all () {
        $.each($(".show_buildresult"), function(index, val) {
          $(val).prop('checked', false);
        });
        toggle_result_all();
      }
      function show_buildresult_all () {
        $.each($(".show_buildresult"), function(index, val) {
          $(val).prop('checked', true);
        });
        toggle_result_all();
      }
      function restore_default_order () {"""
    )
    for name, val in sorted(list(tables["srcgraph"].items())) + sorted(
        list(tables["buildgraph"].items())
    ):
        print("        $('.%s').trigger(\"sorton\",[[[0,%d]]]);" % (name, val["order"]))
    print(
        """      }
      $(document).ready(function() {
        $.each($("table.tablesorter"), function(index, val) {
          var id = $(val).attr('id');
          $(val).tablesorter({widthFixed:true}).tablesorterPager({contain"""
        + """er: $('#' + id + '_pager'), positionFixed: false});
        });
        $('#defaultsort').click(restore_default_order);
        $('#hideall').click(hide_scc_all);
        $('#showall').click(show_scc_all);
        $('#hide_srcresult').click(hide_srcresult_all);
        $('#show_srcresult').click(show_srcresult_all);
        $('#hide_buildresult').click(hide_buildresult_all);
        $('#show_buildresult').click(show_buildresult_all);
        $('#hidesrc').click(toggle_hidesrc);
        $('#hidebuild').click(toggle_hidebuild);"""
    )

    for i, scc in sorted(sccs):
        print(
            "        $('#show_scc_%d').click(function() {toggle_scc(%d);})"
            % ((i + 1), (i + 1))
        )

    for i in sorted(list(tables["srcgraph"].keys())) + sorted(
        list(tables["buildgraph"].keys())
    ):
        print("        $('#show_%s').click(function() {toggle_result('%s');})" % (i, i))

    print(
        """        toggle_hidesrc();
        toggle_hidebuild();
        toggle_scc_all();
        toggle_result_all();
        restore_default_order();
      });
    </script>
  </head>
  <body>
"""
    )
    if args.online:
        print("""    <p>updated: %s</p>""" % (datetime.now().isoformat()))

    print(
        """
    <div id="topbar">
      <div class="topbutton">
        <h1>Table of Contents</h1>
        <ul>
          <li><a href="#srcgraph">Source Graph</a>
            <ul>"""
    )
    for name, val in sorted(
        list(tables["srcgraph"].items()), key=lambda x_y: x_y[1]["title"]
    ):
        print(
            """            <li id="%s_toc"><a href="#%s">%s</a></li>"""
            % (name, name, val["title"])
        )
    print(
        """
            </ul>
          </li>
          <li><a href="#buildgraph">Build Graph</a>
            <ul>"""
    )
    for name, val in sorted(
        list(tables["buildgraph"].items()), key=lambda x_y1: x_y1[1]["title"]
    ):
        print(
            """            <li id="%s_toc"><a href="#%s">%s</a></li>"""
            % (name, name, val["title"])
        )
    print(
        """
            </ul>
          </li>
        <ul>
      </div>
      <div class="topbutton">
        <h1>Global Options</h1>
        <ul>
          <li><input type="checkbox" id="hidesrc" />Hide Source Graph</li>
          <li><input type="checkbox" id="hidebuild" />Hide Build Graph</li>
          <li><input type="button" value="Restore Default Order" id="defau"""
        + """ltsort" /></li>
        </ul>
      </div>
      <div class="topbutton">
        <h1>Toggle Stats</h1>
        <ul>
          <li><input type="button" value="Show All" id="show_buildresult" />
          <input type="button" value="Hide All" id="hide_buildresult" /></li>
          <li>Source Graph
            <ul>"""
    )
    for name, val in sorted(
        list(tables["srcgraph"].items()), key=lambda x_y2: x_y2[1]["title"]
    ):
        print(
            """
            <li><input type="checkbox" checked="checked" class="show_srcre"""
            + """sult" id="show_%s" />Show %s</li>""" % (name, val["title"])
        )
    print(
        """
            </ul>
          </li>
          <li>Build Graph
            <ul>"""
    )
    for name, val in sorted(
        list(tables["buildgraph"].items()), key=lambda x_y3: x_y3[1]["title"]
    ):
        print(
            """
            <li><input type="checkbox" checked="checked" class="show_build"""
            + """result" id="show_%s" />Show %s</li>""" % (name, val["title"])
        )
    print(
        """
            </ul>
          </li>
        </ul>
      </div>
      <div class="topbutton">
        <h1>Toggle SCCs</h1>
        <ul>
          <li><input type="button" value="Show All" id="hideall" />
          <input type="button" value="Hide All" id="showall" /></li>"""
    )
    for i, scc in sccs:
        print(
            """<li><input type="checkbox" %s class="show_scc" id="show_s"""
            % ('checked="checked" ' if i == 0 else "")
            + """cc_%d" />Show SCC #%d (%d vertices)</li>"""
            % (i + 1, i + 1, scc["sg_vertex"])
        )
    print(
        """        </ul>
      </div>
    </div>
    <p>Click on the table headers to sort by that column. Shift+Click allows to
    sort by multiple columns.</p>
    <p>Hover over a package name for architecture and version information.</p>
    <div id="srcgraph">
      <a name="srcgraph"></a>
      <h1>Source Graph</h1>
      <p>The source graph only contains source package vertices and is the"""
        + """refore the
      natural choice for calculating a build order. But it is also the gra"""
        + """ph type
      that can be used to identify strong dependency relationships between"""
        + """ source
      packages. It can therefore be used to identify strong cycles which c"""
        + """annot be
      broken by choosing a different installation set. The smallest possib"""
        + """le cycle is
      the self-cycle of a source package with itself.</p>
      <p>amount of vertices: %d</p>
      <p>amount of edges: %d</p>"""
        % (data["srcgraph"]["snr_vertex"], data["srcgraph"]["snr_edges"])
    )
    for i in sorted(tables["srcgraph"], key=lambda x: tables["srcgraph"][x]["title"]):
        render_section_srcgraph(i, data["srcgraph"])
    print(
        """
    </div>
    <div id="buildgraph">
      <a name="buildgraph"></a>
      <h1>Build Graph</h1>

      <p>The build graph is most important for finding build dependencies to
      remove to make the graph acyclic.</p>

      <p>If you use JavaScript, then by default only statistics of the biggest
      strongly connected component will be displayed to avoid clutter. You can
      activate or deactivate the displaying of statistics for individual
      strongly components by using the respective checkboxes in the "Toggle
      SCCs" menu on the top.</p>

      <p>Just as you can make a selection of the strongly connected components
      you want statistics to be displayed for, you can also make a selection of
      displayed statistic types in the "Toggle Stats" menu on the top.</p>

      <p>amount of vertices: %d</p>
      <p>amount of edges: %d</p>
      <form>"""
        % (data["buildgraph"]["bnr_vertex"], data["buildgraph"]["bnr_edges"])
    )
    print(
        """
          <div style="clear:both">
          </div>"""
    )
    for i, scc in sccs:
        srcstats, binstats = partition(
            scc["srcbinstats"], lambda x: x[0][0].startswith("src:")
        )
        print(
            """
      <div style="width:16em;float:left;" class="scc_%d">
        <form>
          <fieldset>
            <legend>SCC #%d</legend>
            <table>
              <tr><td>amount of vertices</td><td>%s</td></tr>
              <tr><td>amount of edges</td><td>%s</td></tr>
              <tr><td>amount of sources</td><td>%s</td></tr>
              <tr><td>amount of installation sets</td><td>%s</td></tr>
            </table>
          </fieldset>
        </form>
      </div>"""
            % (
                i + 1,
                i + 1,  # id of this scc
                scc["sg_vertex"],  # number of vertices
                scc["sg_edges"],  # number of edges
                len(srcstats),  # number of source packages
                len(binstats),
            )
        )  # number of binary packages
    print(
        """
      <div style="clear:both"></div>
      </form>"""
    )
    for i in sorted(
        tables["buildgraph"], key=lambda x: tables["buildgraph"][x]["title"]
    ):
        render_section_buildgraph(i, sccs)
    print(
        """
    </div>
  </body>
</html>"""
    )


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Turn json statistics into human readable html"
    )
    parser.add_argument(
        "infile", type=read_json_file, help="input statistics in json format"
    )
    parser.add_argument(
        "--online",
        action="store_true",
        help="generate online version (puts timestamp and " + "adjusts links)",
    )
    parser.add_argument("--verbose", action="store_true", help="be verbose")
    args = parser.parse_args()
    ret = stat_html(args.infile, args.online, args.verbose)
