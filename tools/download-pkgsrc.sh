#!/bin/sh -e

download_dist() {
	arch=$1
	name=$2
	packages=$name-$arch-packages.gz
	sources=$name-sources.gz
	if [ \( ! -f $packages \) -o \( ! -f $sources \) ]; then
		case $name in
		2005|2006|2007|2008|2009|2010|2011|2012|2013|2014)
			case $name in
				#2005) date=20050312T000000Z ;; # there are no gz
				#2006) date=20060101T000000Z ;; # in 2005 and 2006
				2007) date=20070101T000000Z ;;
				2008) date=20080101T000000Z ;;
				2009) date=20090101T000000Z ;;
				2010) date=20100101T000000Z ;;
				2011) date=20110101T000000Z ;;
				2012) date=20120101T000000Z ;;
				2013) date=20130101T000000Z ;;
				2014) date=20140101T000000Z ;;
			esac
			packages=sid-$arch-packages-$date.gz
			sources=sid-sources-$date.gz
			if [ ! -f $packages ]; then
			        wget -c http://snapshot.debian.org/archive/debian/$date/dists/sid/main/binary-$arch/Packages.gz -O $packages
			fi
			if [ ! -f $sources ]; then
			        wget -c http://snapshot.debian.org/archive/debian/$date/dists/sid/main/source/Sources.gz -O $sources
			fi
			;;
		etch|lenny)
			wget -c http://http.debian.net/debian-archive/debian/dists/$name/main/binary-$arch/Packages.gz -O $packages
			wget -c http://http.debian.net/debian-archive/debian/dists/$name/main/source/Sources.gz -O $sources
			;;
		squeeze|wheezy|jessie|sid)
			wget -c http://http.debian.net/debian/dists/$name/main/binary-$arch/Packages.gz -O $packages
			wget -c http://http.debian.net/debian/dists/$name/main/source/Sources.gz -O $sources
			;;
		hardy|lucid|precise)
			wget -c http://de.archive.ubuntu.com/ubuntu/dists/$name/main/binary-$arch/Packages.gz -O $packages
			wget -c http://de.archive.ubuntu.com/ubuntu/dists/$name/main/source/Sources.gz -O $sources
			;;
		*)
			echo "unknown distribution"
			exit 1;;
		esac
	fi
	echo "$arch $packages $sources"
}

download_default() {
	download_dist amd64 2014
}

case $# in
0) download_default;;
2) download_dist $1 $2;;
*) echo "invalid argument number";
	echo "usage: $0 [arch suite]"
	exit 1;;
esac


