#!/usr/bin/env python3

from __future__ import print_function
import sys

sys.path.append("/usr/share/botch")
from util import read_reduced_deps

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description=(
            "Display the difference between two files with "
            + "droppable build dependencies"
        )
    )
    parser.add_argument("droppable1", type=read_reduced_deps, help="file 1")
    parser.add_argument("droppable2", type=read_reduced_deps, help="file 2")
    parser.add_argument("-v", "--verbose", action="store_true", help="be verbose")
    args = parser.parse_args()

    only_in_1 = set(args.droppable1.keys()) - set(args.droppable2.keys())
    only_in_2 = set(args.droppable2.keys()) - set(args.droppable1.keys())
    in_both = set(args.droppable1.keys()) & set(args.droppable2.keys())

    for srcpkg in in_both:
        d1 = set(args.droppable1[srcpkg])
        d2 = set(args.droppable2[srcpkg])
        o_i_1 = d1 - d2
        o_i_2 = d2 - d1
        if not o_i_1 and not o_i_2:
            continue
        print(srcpkg)
        if o_i_1:
            print("< %s" % (" ".join(o_i_1)))
        if o_i_2:
            print("> %s" % (" ".join(o_i_2)))

    if only_in_1:
        print("only in <: %s", ",".join(only_in_1))

    if only_in_2:
        print("only in >: %s", ",".join(only_in_2))
