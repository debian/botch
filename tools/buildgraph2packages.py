#!/usr/bin/env python3

import sys

sys.path.append("/usr/share/botch")
from util import read_graphml, read_tag_file, sort_pkgs_key
import re


def extract_name_arch_ver(pkgrel):
    m = re.match(r"([^:]*):([^ ]*) \(= ([^\)]*)\)", pkgrel)
    return m.groups()


def buildgraph2packages(buildgraph, inPackages):
    universe = dict()

    for packages in inPackages:
        for pkg in packages:
            key = (pkg["Package"], pkg["Architecture"], pkg["Version"])
            universe[key] = pkg

    result = set()

    for n, d in buildgraph.nodes(data=True):
        if d.get("type") == "bin":
            # Do not add the binary package this IS belongs to.
            # Only add the binary packages in the installation set.
            # For normal graphs and closure graphs this has no
            # effect because the binary package is part of the IS
            # but for strong graphs the binary package might not
            # be part of the IS
            for pkg in d["binaries"].split(","):
                name, arch, version = extract_name_arch_ver(pkg)
                result.add((name, arch, version))

    for pkg in sorted(list(result), key=sort_pkgs_key):
        universe[pkg].dump(sys.stdout.buffer)
        sys.stdout.buffer.write(b"\n")

    return True


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description=(
            "Output all binary packages occurring in all"
            + " installation sets in a build graph"
        )
    )
    parser.add_argument(
        "buildgraph", type=read_graphml, help="Input buildgraph in GraphML format"
    )
    parser.add_argument(
        "packages", type=read_tag_file, nargs="+", help="input Packages files"
    )
    parser.add_argument("-v", "--verbose", action="store_true", help="be verbose")
    args = parser.parse_args()
    ret = buildgraph2packages(args.buildgraph, args.packages)
    exit(not ret)
