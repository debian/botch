botch (0.24-6) unstable; urgency=medium

  * debian/salsa-ci.yml: modernize
  * debian/patches:
     - new version of ocaml-atd
     - refresh parsing of argparse --help output in python 3.13 (closes:
       #1092479)

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Thu, 13 Feb 2025 23:30:00 +0100

botch (0.24-5) unstable; urgency=medium

  [ Stéphane Glondu ]
  * Use Stdlib instead of Pervasives  (Closes: #1078356)

  [ Johannes Schauer Marin Rodrigues ]
  * debian/control: add Recommends on wget (Closes: #928682)
  * debian/control: fix Hompage to point to salsa (Closes: #988411)

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Sun, 11 Aug 2024 11:22:45 +0200

botch (0.24-4) unstable; urgency=medium

  * Team upload (ack by maintainer)
  * Depend on yojson-tools instead of old libyojson-ocaml-dev
    to fix missing ydump tool (split from libyojson-ocaml-dev)
    This fixes autopkgtests and build-failure
    (Closes: #1073199, #1073259)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 14 Jun 2024 14:01:21 +0200

botch (0.24-3) unstable; urgency=medium

  * debian/control: switch out unmaintained markdown for discount

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Mon, 10 Jun 2024 12:40:37 +0200

botch (0.24-2) unstable; urgency=medium

  * add patch to make tests work with black 23.1.0 (closes: #1031469)

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Fri, 17 Feb 2023 15:49:11 +0100

botch (0.24-1) unstable; urgency=medium

  * New upstream release
     - port to dose3 7.0.0 (closes: #1006049)
  * debian/watch: bump version to 4
  * debian/control: bump dh version to 13
  * debian/control: bump Standards-Version to 4.5.1
  * debian/botch.install: list manpages
  * fix my name
  * drop patches
  * remove parmap and parany
  * add patch fixing Bi_outbuf -> Buffer
  * patch json test formatting
  * debian/tests: remove --jobs
  * debian/source/lintian-overrides: override some lintian false-positives
  * debian/salsa-ci.yml: disable crossbuilding

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Mon, 31 Oct 2022 12:51:07 +0100

botch (0.23-1) unstable; urgency=medium

  * New upstream release
  * drop patches and add patch replacing parany with parmap
  * add patch to fix expected html in tests

 -- Johannes 'josch' Schauer <josch@debian.org>  Fri, 05 Feb 2021 01:51:43 +0100

botch (0.22-5) unstable; urgency=medium

  * patch tools/stat-html.py to make black happy (closes: #972258)
  * add debian/patches/testsuite-fixes-new-ocaml.patch because of changes in
    dose3 and ocaml since the last upload

 -- Johannes 'josch' Schauer <josch@debian.org>  Sat, 17 Oct 2020 16:53:50 +0200

botch (0.22-4) unstable; urgency=medium

  * add patch to fix pyflakes failure (closes: #963399)
  * add patch to fix pycodestyle error E741

 -- Johannes 'josch' Schauer <josch@debian.org>  Mon, 22 Jun 2020 11:40:47 +0200

botch (0.22-3) unstable; urgency=medium

  * add debian/salsa-ci.yml
  * change vcs to salsa because of #914260
  * debian/upstream/signing-key.asc: drop extra signatures
  * debian/control: bump standards-version to 4.5.0
  * debian/tests/misc: remove out/acl_2.2.52-3_n20.svg and
    out/doxygen_1.8.11-3.svg because graphviz stores its version in it

 -- Johannes 'josch' Schauer <josch@debian.org>  Sun, 23 Feb 2020 08:38:11 +0100

botch (0.22-2) unstable; urgency=medium

  * Add patch as workaround for bug #946980. On 32bit architectures, botch
    will now not attempt using parmap for parallelization (closes: #946817)
  * Fixup last changelog entry that forgot a closes: #944473

 -- Johannes 'josch' Schauer <josch@debian.org>  Thu, 02 Jan 2020 15:02:44 +0100

botch (0.22-1) unstable; urgency=medium

  * New upstream release
  * drop debian/patches -> all have been applied upstream
  * drop B-D on libcdt5 because tests are not testing graphviz version anymore
    (closes: #944473)
  * replace deprecated $ADTTMP with $AUTOPKGTEST_TMP
  * bump Standards-Version to 4.4.1 (no changes)
  * add Rules-Requires-Root: no
  * bump debhelper version to 12
  * use debhelper-compat B-D instead of debian/compat
  * add B-D: black <!nocheck>

 -- Johannes 'josch' Schauer <josch@debian.org>  Sun, 15 Dec 2019 05:28:44 +0100

botch (0.21-8) unstable; urgency=medium

  * add patch accounts for new argparse version in python 3.7 (closes: #912935)

 -- Johannes 'josch' Schauer <josch@debian.org>  Thu, 22 Nov 2018 15:47:58 +0100

botch (0.21-7) unstable; urgency=medium

  * also ignore pycodestyle W504 (closes: #912060)

 -- Johannes 'josch' Schauer <josch@debian.org>  Wed, 21 Nov 2018 08:57:07 +0100

botch (0.21-6) unstable; urgency=medium

  * debian/control: add Build-Depends on dh-python (closes: #896797)
  * Define undefined parameters in shell scripts (closes: #903564)
  * Migrate to networkx 2.0
  * update for graphviz 2.40.1 (closes: #890029)
  * Fix for changed version provides handling of dose3 5.0.1-9
  * tools/graph-difference.py: allow comparison of arbitrary input graphs
  * Rename ma_interpreter.pkggraph to ma_interpreter.xml so that it is checked
    using botch-graph-difference instead of diff
  * debian/control: fix my name
  * bump dh compat level to 11
  * debian/copyright: secure source format uri

 -- Johannes 'josch' Schauer <josch@debian.org>  Sun, 22 Jul 2018 19:07:41 +0200

botch (0.21-5) unstable; urgency=medium

  * add patch fixing test output for changed dictionary ordering from Python
    3.5 to 3.6. Also bump build dependency on Python (closes: #869586)
  * remove empty recommends and suggests fields from botch-doc
  * use https URI in debian/watch
  * bump Standards-Version to 4.1.1 (no changes)

 -- Johannes Schauer <josch@debian.org>  Wed, 01 Nov 2017 11:13:50 +0100

botch (0.21-4) unstable; urgency=medium

  * Add patch to fix FTBFS due to new pycodestyle and error E722 (closes:
    #867005)
  * Replace jquery-tablesorter/themes/blue/style.css with
    jquery-tablesorter/css/theme.blue.css after upstream switch (closes:
    #867607)
  * Build-Depends on ocamlbuild to ease a future transition to ocaml 4.05.0
    (closes: #869852)
  * bump Standards-Version to 4.0.0 (no changes required)
  * make botch-doc Multi-Arch:foreign

 -- Johannes Schauer <josch@debian.org>  Wed, 02 Aug 2017 13:13:10 +0200

botch (0.21-3) unstable; urgency=medium

  * Add patch to fix FTBFS due to API breakage introduced by src:dose3 5.0.1-8
    (closes: #852917)

 -- Johannes Schauer <josch@debian.org>  Sat, 28 Jan 2017 23:32:39 +0100

botch (0.21-2) unstable; urgency=medium

  * python3-pygraphviz dropped the file attribute. Thus adapting the code
    accordingly and bumping dependency to 1.4~rc1
  * bump Standards-Version to 3.9.8 (no changes required)
  * Do not build depend on -1-revisions to ease backports
  * Remove unused lintian overrides

 -- Johannes Schauer <josch@debian.org>  Mon, 09 Jan 2017 13:16:33 +0100

botch (0.21-1) unstable; urgency=medium

  * new upstream release
     - changes testcases for gringo 5.1.0 (closes: #847831)
  * remove all patches
  * fail if diff in autopkgtest returns nonzero exit (closes: #847832)
  * move diff_tmp_out of autopkgtest into its own script

 -- Johannes Schauer <josch@debian.org>  Tue, 20 Dec 2016 19:45:42 +0100

botch (0.20-3) unstable; urgency=medium

  * Add two patches from upstream which switch from pep8 to pycodestyle
    (closes: #844964)

 -- Johannes Schauer <josch@debian.org>  Sat, 19 Nov 2016 20:55:30 +0100

botch (0.20-2) unstable; urgency=medium

  * Apply patch from upstream catering for missing 'depends' field in dose3
    yaml output

 -- Johannes Schauer <josch@debian.org>  Tue, 01 Nov 2016 08:05:41 +0100

botch (0.20-1) unstable; urgency=medium

  * new upstream release

 -- Johannes Schauer <josch@debian.org>  Mon, 31 Oct 2016 12:28:52 +0100

botch (0.19-3) unstable; urgency=medium

  * Update autopkgtest test suite to 0.19 release

 -- Johannes Schauer <josch@debian.org>  Tue, 25 Oct 2016 22:34:25 +0200

botch (0.19-2) unstable; urgency=medium

  * Run ocamlbuild with -lflags -runtime-variant,_pic to work around Debian
    bug #837359

 -- Johannes Schauer <josch@debian.org>  Thu, 20 Oct 2016 07:17:42 +0200

botch (0.19-1) unstable; urgency=medium

  * new upstream release
  * drop patches as they were integrated upstream
  * add a Suggests:botch-doc to botch (closes: #839924)
  * make the botch package description less verbose (closes: #839926)
  * fix VCS-Git field to point to the botch git (closes: #839901)
  * add a README.Debian
  * update lintian overrides (remove hardening-no-relro and add
    hardening-no-bindnow and hardening-no-pie)

 -- Johannes Schauer <josch@debian.org>  Wed, 19 Oct 2016 23:22:38 +0200

botch (0.18-5) unstable; urgency=medium

  * dummy upload to test source-only uploads without an empty Binary field in
    the .changes file

 -- Johannes Schauer <josch@debian.org>  Thu, 22 Sep 2016 12:44:15 +0200

botch (0.18-4) unstable; urgency=medium

  * backport patch to output progress for botch-clean-repository via
    0001-clean-repository.ml-add-progress-option.patch

 -- Johannes Schauer <josch@debian.org>  Thu, 22 Sep 2016 12:37:57 +0200

botch (0.18-3) unstable; urgency=medium

  * backport patch which applies _tag rules to *.byte instead of just applying
    them to *.d.byte via
    0002-_tags-don-t-apply-rule-to-.d.byte-only-but-to-.byte.patch
  * build-depend on libatdgen-ocaml-dev (>= 1.9.1-2) to make sure to depend on
    a version of atdgen with bug #837804 fixed (provides /usr/bin/atdgen on
    all architectures)

 -- Johannes Schauer <josch@debian.org>  Fri, 16 Sep 2016 09:58:08 +0200

botch (0.18-2) unstable; urgency=medium

  * backport patch that retries parmap with smaller chunk sizes to cater for
    32 bit architectures with very limited memory constraints
    0001-buildGraph.ml-retry-with-smaller-chunk-sizes-if-parm.patch

 -- Johannes Schauer <josch@debian.org>  Fri, 16 Sep 2016 07:06:02 +0200

botch (0.18-1) unstable; urgency=medium

  * new upstream release
      - restores compatibility with dose3 (closes: #827977)
  * add dependency on dpkg-dev (closes: #832028)
  * remove all patches
  * replace build-depends on pyflakes by pyflakes3
  * replace dependcy on python3-pydot by python3-pygraphviz
  * update autopkgtests
  * add dependency on libyojson-ocaml-dev for the ydump program
  * bump minimum dose3 version to 5.0.1-1

 -- Johannes Schauer <josch@debian.org>  Mon, 12 Sep 2016 09:35:46 +0200

botch (0.17-3) unstable; urgency=medium

  * Allows one to only build architecture dependent or architecture
    independent packages (Closes: #804296)
      - build dependencies are selected through Build-Depends-Arch and
        Build-Depends-Indep
      - this only fixes #804296 because it will not build the failing
        wiki target anymore during architecture dependent builds on mips or
        mipsel. The real fix still has to be done in #804297. The problem is
        hidden because Architecture:all packages are not built on mips or
        mipsel.
  * support for DEB_BUILD_PROFILES nodoc and nocheck
  * support for DEB_BUILD_OPTIONS nodoc and nocheck

 -- Johannes Schauer <josch@debian.org>  Sun, 08 Nov 2015 14:04:18 +0100

botch (0.17-2) unstable; urgency=medium

  * backport two patches from upstream git to support running the misc unit
    test target on non-native architectures:
       - 797639f Makefile: do not add debug information by default on
         non-native architectures, move that to a new debug target
       - bc22129 tools/*.sh: check all possible ocaml extensions to find an
         executable

 -- Johannes Schauer <josch@debian.org>  Fri, 06 Nov 2015 14:40:46 +0100

botch (0.17-1) unstable; urgency=medium

  * new upstream release
  * The upload of clasp 3.1.3-1 broke aspcud in some cases (see #800526 and
    #801169) and introduced a spurious failure of the test suite that was
    worked around with the upload of upstream version 0.16. Now that the
    problem is known and clasp was fixed with 3.1.3-2, this upstream release
    reverts that change (closes: #802265).
  * remove debian/patches/print-stats.ml-do-not-use-yojson.safe.pr because
    that commit is part of this upstream release
  * refresh debian/patches/send-keepalive
  * add debian/tests/misc
  * add build dependency on python3-pydot for the test suite
  * add build dependency on gsfonts because graphviz wants a font called
    "Times-Roman" and otherwise falls back to "DejaVu Serif" instead of using
    "Nimbus Roman No9 L" with gsfonts installed. This could've also been
    fixed by changing the testcase png be rendered using "DejaVu Serif"
  * add build dependency on libgraph-easy-perl for the graph-easy tool which
    is needed by y-u-b-d-transitive-essential in the test suite
  * break long lines in debian/control for extended description
  * add additional Depends for debian/tests/control for the misc test

 -- Johannes Schauer <josch@debian.org>  Tue, 03 Nov 2015 19:40:27 +0100

botch (0.16-2) unstable; urgency=medium

  * fix build problem on 32bit architectures

 -- Johannes Schauer <josch@debian.org>  Sun, 27 Sep 2015 13:20:46 +0200

botch (0.16-1) unstable; urgency=medium

  * New upstream release

 -- Johannes Schauer <josch@debian.org>  Sat, 26 Sep 2015 19:28:51 +0200

botch (0.15-4) unstable; urgency=medium

  * and another one...

 -- Johannes Schauer <josch@debian.org>  Sat, 26 Sep 2015 16:10:02 +0200

botch (0.15-3) unstable; urgency=medium

  * another dgit test upload

 -- Johannes Schauer <josch@debian.org>  Sat, 26 Sep 2015 16:02:40 +0200

botch (0.15-2) unstable; urgency=medium

  * dgit test upload

 -- Johannes Schauer <josch@debian.org>  Sat, 26 Sep 2015 14:08:42 +0200

botch (0.15-1) unstable; urgency=medium

  * New upstream release (Closes: #797453)
  * bump dependency on dose tools >= 4.0.1-2
  * remove dependency on jq because it's not needed anymore due to new dose
    tools
  * update botch package description
  * update Vcs-* fields to use dgit

 -- Johannes Schauer <josch@debian.org>  Sat, 26 Sep 2015 08:48:32 +0200

botch (0.14-1) unstable; urgency=medium

  * New upstream release
  * refresh patch send-keepalive
  * botch binary: add dependency on python3-pydot, zutils and suggest
    libgraph-easy-perl, jq
  * update debian/tests/* with --drop-b-d-indep argument and correct locale
    environment variable export
  * make debian/tests/* more readable by splitting long lines
  * update debian/tests/* with workaround for random segmentation faults
    see http://bugs.python.org/issue24605

 -- Johannes Schauer <josch@debian.org>  Wed, 29 Jul 2015 10:10:38 +0200

botch (0.13-1) unstable; urgency=medium

  * New upstream release (closes: #784835)
  * Remove the XS-Testsuite field from d/control to fix lintian warning
    xs-testsuite-header-in-debian-control

 -- Johannes Schauer <josch@debian.org>  Sun, 07 Jun 2015 12:17:15 +0200

botch (0.12-1) unstable; urgency=medium

  * New upstream release (closes: #764704)

 -- Johannes Schauer <josch@debian.org>  Wed, 03 Jun 2015 20:58:37 +0200

botch (0.11-2) unstable; urgency=medium

  * fix last d/changelog entry to say unstable instead of UNRELEASED
    uploads like this should be rejected in the first place. See #647028

 -- Johannes Schauer <josch@debian.org>  Thu, 07 May 2015 08:10:33 +0200

botch (0.11-1) unstable; urgency=medium

  * New upstream release

 -- Johannes Schauer <josch@debian.org>  Wed, 06 May 2015 07:52:20 +0200

botch (0.10-1~experimental1) experimental; urgency=medium

  * New upstream release
  * bump dependency on libdose to 3.3-2 because of its backported fixes for
    fd-leakage and storing the original Debian package name as a cudf_extras
    field

 -- Johannes Schauer <josch@debian.org>  Fri, 27 Mar 2015 12:07:59 +0100

botch (0.9-1~experimental1) experimental; urgency=medium

  * New upstream release
  * add build dependency on libparmap-ocaml-dev

 -- Johannes Schauer <josch@debian.org>  Mon, 23 Mar 2015 21:34:37 +0100

botch (0.8-1~experimental1) experimental; urgency=medium

  * New upstream release

 -- Johannes Schauer <josch@debian.org>  Fri, 20 Mar 2015 17:08:34 +0100

botch (0.7-1~experimental1) experimental; urgency=medium

  * New upstream release
  * replace gitorious upstream url with gitlab.mister-muffin.de

 -- Johannes Schauer <josch@debian.org>  Fri, 20 Mar 2015 08:17:29 +0100

botch (0.6-1~experimental3) experimental; urgency=medium

  * add patch send-keepalive to not abort build on arches without ocamlopt

 -- Johannes Schauer <josch@debian.org>  Sun, 15 Feb 2015 09:55:37 +0100

botch (0.6-1~experimental2) experimental; urgency=medium

  * do not explicitly set `dh_gencontrol -- -Vocaml:Depends` already done by
    dh-ocaml
  * do not run test suite without ocamlopt as that will make the build get
    killed by a timeout (closes: #778449)
  * add dh --with ocaml
  * include /usr/share/ocaml/ocamlvars.mk for OCAML_HAVE_OCAMLOPT
  * Build-Depend on dh-ocaml
  * add lintian overrides for hardening and strip errors

 -- Johannes Schauer <josch@debian.org>  Sun, 15 Feb 2015 08:04:35 +0100

botch (0.6-1~experimental1) experimental; urgency=medium

  * New upstream release (closes: #778373)

 -- Johannes Schauer <josch@debian.org>  Sat, 14 Feb 2015 10:56:50 +0100

botch (0.5-1~experimental3) experimental; urgency=medium

  * this upload removes the build-dependency on python3-yaml:native and the
    dependency on python3-yaml:amd64 to make it compilable/installable again

 -- Johannes Schauer <josch@debian.org>  Thu, 12 Feb 2015 10:57:05 +0100

botch (0.5-1~experimental2) experimental; urgency=medium

  * making an upload to experimental

 -- Johannes Schauer <josch@debian.org>  Fri, 06 Feb 2015 10:45:02 +0100

botch (0.5-1~experimental1) unstable; urgency=medium

  * New upstream release.
    - graph comparison is now done using botch-graph-difference
      (Closes: #764909)
    - adjusted to build with latest dose 3.3 (Closes: #770613)
  * bump dependency on dose lib and tools to 3.3
  * adjust autopkgtests to use botch-graph-difference to compare graphs
  * bump Standards-Version to 3.9.6
  * botch now suggests www-browser because some of its output is HTML
  * change my email in Maintainer field to josch@debian.org
  * add Vcs-Browser and Vcs-Git fields
  * change priority from extra to optional because it has no conflicts and is
    not a debug package
  * add build dependency on python3-yaml:native and dependency on
    python3-yaml:amd64 for testing purposes - please revert!

 -- Johannes Schauer <josch@debian.org>  Fri, 06 Feb 2015 07:26:47 +0100

botch (0.4-1) unstable; urgency=medium

  * New upstream release.

 -- Johannes Schauer <j.schauer@email.de>  Tue, 14 Oct 2014 01:26:25 +0200

botch (0.3-1) unstable; urgency=medium

  * New upstream release.

 -- Johannes Schauer <j.schauer@email.de>  Sun, 12 Oct 2014 08:51:01 +0200

botch (0.2-1) unstable; urgency=medium

  * New upstream release.

 -- Johannes Schauer <j.schauer@email.de>  Sun, 12 Oct 2014 02:03:02 +0200

botch (0.1-1) unstable; urgency=medium

  * Initial release. (Closes: #748102)

 -- Johannes Schauer <j.schauer@email.de>  Wed, 14 May 2014 11:54:59 +0200
