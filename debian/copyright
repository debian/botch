Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: botch
Upstream-Contact: Johannes Schauer Marin Rodrigues <josch@debian.org>
Source: https://salsa.debian.org/debian-bootstrap-team/botch/

Files: *
Copyright: 2012-2022 Johannes Schauer Marin Rodrigues <josch@debian.org>
           2012-2014 Pietro Abate <pietro.abate@pps.jussieu.fr>
License: LGPL-3+ with OCaml-linking exception

Files: debian/*
Copyright: 2014-2022 Johannes Schauer Marin Rodrigues <josch@debian.org>
License: LGPL-3+ with OCaml-linking exception

Files: tools/wanna-build-sortblockers.py
Copyright: 2014 Chen Baozi <baozich@gmail.com>
License: GPL-2

Files: tools/build-order-from-zero.py
Copyright: 2014 Helmut Grohne <helmut@subdivi.de>
License: Expat or LGPL-3+ with OCaml-linking exception

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: LGPL-3+ with OCaml-linking exception
 This library is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.  A special linking
 exception to the GNU Lesser General Public License applies to this
 library, see the COPYING file for more information.
 .
 On Debian systems, the complete text of the latest GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-3'.

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 2 as
 published by the Free Software Foundation.
 .
 On Debian systems, the complete text of the latest GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.
