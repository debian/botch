Source: botch
Section: utils
Priority: optional
Maintainer: Johannes Schauer Marin Rodrigues <josch@debian.org>
Homepage: https://salsa.debian.org/debian-bootstrap-team/botch/-/wikis/home
Standards-Version: 4.5.1
Rules-Requires-Root: no
# these are needed to run debian/rules clean
Build-Depends:
 debhelper-compat (= 13),
 dh-ocaml,
 dh-python,
 ocaml-nox,
 ocamlbuild,
 python3 (>= 3.6),
Build-Depends-Arch:
 cppo,
 ocaml-findlib,
 libextlib-ocaml-dev,
 libocamlgraph-ocaml-dev,
 libatdgen-ocaml-dev (>= 1.9.1-2),
 libxmlm-ocaml-dev,
 libdose3-ocaml-dev (>= 5.0.1),
 dose-builddebcheck (>= 5.0.1-10) <!nocheck>,
 dose-extra (>= 5.0.1)           <!nocheck>,
 dctrl-tools                     <!nocheck>,
 aspcud                          <!nocheck>,
 python3-apt                     <!nocheck>,
 python3-debian                  <!nocheck>,
 python3-networkx (>= 2.1)       <!nocheck>,
 python3-yaml                    <!nocheck>,
 pycodestyle                     <!nocheck>,
 pyflakes3                       <!nocheck>,
 python3-pygraphviz (>= 1.4~rc1) <!nocheck>,
 gsfonts                         <!nocheck>,
 libgraph-easy-perl              <!nocheck>,
 graphviz                        <!nocheck>,
 black                           <!nocheck>,
 yojson-tools                    <!nocheck>,
Build-Depends-Indep:
 discount <!nodoc>,
 graphviz <!nodoc>,
Vcs-Browser: https://salsa.debian.org/debian/botch
Vcs-Git: https://salsa.debian.org/debian/botch.git

Package: botch
Architecture: any
Depends:
 ${ocaml:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
 ${python3:Depends},
 dose-builddebcheck (>= 5.0.1-1),
 dose-extra (>= 5.0.1-1),
 dctrl-tools,
 aspcud,
 python3-apt,
 python3-debian,
 python3-networkx (>= 2.1-1),
 python3-yaml,
 python3-pygraphviz (>= 1.4~rc1),
 zutils,
 dpkg-dev,
 yojson-tools,
# libjs-jquery-tablesorter and libjs-jquery are needed to look at the generated
# HTML with javascript bling
Recommends:
 wget,
 libjs-jquery-tablesorter,
 libjs-jquery
# devscripts and libsoap-lite-perl are needed for /usr/bin/bts which is in turn
# needed by botch-dose2html because there is no soap module for python3
# libgraph-easy-perl is only needed for its /usr/bin/graph-easy program which
# is used in y-u-no-bootstrap and y-u-b-d-transitive-essential (but optional)
Suggests:
 libgraph-easy-perl,
 devscripts,
 www-browser,
 libsoap-lite-perl,
 botch-doc,
Description: tools to create and analyse dependency graphs
 botch is a collection of tools to create and analyze dependency graphs. This
 task is facilitated by a range of individual tools which use the deb822 and
 graphml formats to exchange information with each other.
 .
 The tools installed by this package provide these features:
 .
  - analyze graphs
  - transform Packages and Sources control files
  - create graphs of different types
  - analysis of Packages and Sources control files
  - handling of botch-internal formats
  - dose3 wrappers
  - set operations on Packages or Sources files
  - create build orders
  - conversion of graphs
  - shell scripts connecting the tools for meaningful operations
  - extract regions from graphs

Package: botch-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: Bootstrapping helper - documentation
 Botch stands for bootstrap/build ordering tool chain and allows one to create
 and analyze bootstrapping dependency graphs, creates suggestions how to break
 dependency cycles and generates a build order.
 .
 This package contains the documentation.
