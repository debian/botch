Botch stands for Bootstrap/Build Order ToolCHain. It consists of a number of tools that aid in breaking dependency cycles and generating a build order so that Debian can be bootstrapped.

# Source code #

    git clone http://gitlab.mister-muffin.de/debian-bootstrap/botch.git

# Documentation #

 * [First Steps](FirstSteps)
 * [Tools](Tools)
 * [Terminology](Terminology)
 * [Heuristics](Heuristics)
 * [Examples/Graphs](ExamplesGraphs)
 * [Master Thesis about botch](http://mister-muffin.de/bootstrap/thesis.pdf)
 * [FOSDEM Talk](https://fosdem.org/2013/schedule/event/debian_bootstrap/) ([Slides](http://mister-muffin.de/bootstrap/fosdem2013.pdf)) ([Video](http://video.fosdem.org/2013/crossdistro/Bootstrapping_Debian_based_distributions_for_new_architectures.webm), [Mirror](http://mister-muffin.de/bootstrap/fosdem2013.webm))
 * [DebConf13 Talk](http://penta.debconf.org/dc13_schedule/events/984.en.html) ([Slides](http://mister-muffin.de/bootstrap/debconf13.pdf)) ([Video](http://meetings-archive.debian.net/pub/debian-meetings/2013/debconf13/webm-high/984_An_introduction_to_the_BootstrapBuild_Ordering_Toolchain.webm), [Mirror](http://mister-muffin.de/bootstrap/debconf13.webm))
 * [CBSE2013](http://comparch2013.org/program/cbse.html) ([Paper](http://mister-muffin.de/bootstrap/cbse2013.pdf)) ([Slides](http://mister-muffin.de/bootstrap/cbse2013slides.pdf)) ([Video](http://mister-muffin.de/bootstrap/cbse2013talk.mkv))
 * [Informatiktage 2013](http://informatiktage.gi.de/informatiktage/rueckblicke/informatiktage-2013/wissenschafts-und-absolventenworkshop.html) ([Paper](http://mister-muffin.de/bootstrap/informatiktage2013.pdf#page=96))

# Contact #

 * Email: `j.schauer@email.de`
 * IRC: `#debian-bootstrap` on `irc.debian.org`

# Links #

 * [General Bootstrapping Introduction on the Debian wiki](http://wiki.debian.org/DebianBootstrap)
 * [TODO list for developers](http://wiki.debian.org/DebianBootstrap/TODO)
 * [Blog](http://blog.mister-muffin.de)