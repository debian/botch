=head1 NAME

botch-fix-cross-problems - fix Packages files with the output of B<botch-cross-problems>

=head1 SYNOPSIS

=over

=item B<botch-fix-cross-problems> [options] I<dose.yaml> I<buildpackages> I<hostpackages>

=back

=head1 DESCRIPTION

fix build and host architecture Packages files according to the result of
botch-dose2html

=head1 OPTIONS

=over 4

=item B<-h, --help>

Show help.

=item B<-v, --verbose>

Be verbose.

=back

=head1 BUGS

See L<http://bugs.debian.org/botch>.

=head1 SEE ALSO

Debian doc-base Manual F</usr/share/doc/botch/wiki/Home.html>

=head1 AUTHOR

This man page was written by Johannes Schauer. Botch is written by Johannes
Schauer and Pietro Abate.

=head1 COPYRIGHT

Copyright 2012-2014 Johannes Schauer, Pietro Abate

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version. A special linking exception to the GNU Lesser General Public
License applies to this library, see the COPYING file for more information.

