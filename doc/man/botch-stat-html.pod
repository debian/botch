=head1 NAME

botch-stat-html - turn the JSON output of B<botch-print-stats> into human readable HTML

=head1 SYNOPSIS

=over

=item B<botch-stat-html> [options] I<infile>

=back

=head1 DESCRIPTION

Turn json statistics into human readable html.

=head1 OPTIONS

=over 4

=item B<-h, --help>

Show help.

=item B<--verbose>

Be verbose.

=item B<--online>

Generate online version (puts timestamp and adjusts links).

=back

=head1 BUGS

See L<http://bugs.debian.org/botch>.

=head1 SEE ALSO

Debian doc-base Manual F</usr/share/doc/botch/wiki/Home.html>

=head1 AUTHOR

This man page was written by Johannes Schauer. Botch is written by Johannes
Schauer and Pietro Abate.

=head1 COPYRIGHT

Copyright 2012-2014 Johannes Schauer, Pietro Abate

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version. A special linking exception to the GNU Lesser General Public
License applies to this library, see the COPYING file for more information.

