=head1 NAME

botch-create-graph - create graph based on installation sets, strong dependency sets or dependency closures

=head1 SYNOPSIS

=over

=item B<botch-create-graph> [options] I<Packages..> I<Sources>

=back

=head1 DESCRIPTION

Given a repository of source packages and binary packages, create the build
graph and source graph for it

=head1 OPTIONS

=over 4

=item B<--version>

show program's version and exit

=item B<-h, --help>

show this help message and exit

=item B<-v, --verbose>

print additional information

=item B<--quiet>

do no print any messages

=item B<-A, --available=>I<FILE>

List of available packages (arch:all, crossed...) in control file format

=item B<--custom-is=>I<FILE>

list of files with dependencies that should not be part of the IS

=item B<--deb-drop-b-d-indep>

Drop Build-Depends-Indep dependencies

=item B<--deb-drop-b-d-arch>

Drop Build-Depends-Arch dependencies

=item B<--allowsrcmismatch>

If a binary package is without a source package but there is a source package
of same name but different version, match this binary package to that source
package.

=item B<--progress>

print progress bars

=item B<--timers>

print timing information

=item B<--strongtype>

generate a strong build graph (only strong dependencies)

=item B<--closuretype>

generate a dependency closure build graph (all dependency relationships)

=item B<--fg=>I<PKGS>

Additional Packages lists that are checked and used for resolving dependencies
(can be repeated)

=item B<--bg=>I<PKGS>

Additional Packages lists that are NOT checked but used for resolving
dependencies (can be repeated)

=item B<--deb-native-arch=>I<ARCH>

Native architecture I<ARCH>

=item B<--deb-host-arch=>I<arch>

Host architecture I<arch>. Defaults to native architecture.

=item B<--deb-foreign-archs=>I<archs>

Comma-separated foreign architectures in addition to native and host
architectures.

=item B<--deb-ignore-essential>

By default, installing any binary package requires the coinstallation set of
all binary packages marked as Essential:yes to be installed as well. This
option drops this requirement.

=item B<--optgraph>

Calculate a dependency graph where each installation set contains the minimal
number of unavailable binary packages.

=back

=head1 EXAMPLE

Calculate a self-contained repository:

 zcat packages | grep-dctrl -X \( -FPackage build-essential --or -FEssential yes \) > minimal
 dose-deb-coinstall --deb-native-arch=amd64 --bg=packages --fg=minimal > minimal-amd64
 botch-bin2src --deb-native-arch=amd64 minimal-amd64 sources > minimal-src
 grep-dctrl -FArchitecture all packages > available
 botch-create-graph --deb-ignore-essential -A available --deb-native-arch=amd64 --bg sources packages minimal-src > graph.xml
 botch-buildgraph2packages graph.xml packages > min-closure
 botch-packages-union min-closure minimal-amd64 min-closure
 botch-bin2src --deb-native-arch=amd64 min-closure sources > min-closure-src
 dose-builddebcheck --deb-native-arch=amd64 min-closure min-closure-src

The self-contained repository will then be I<min-closure> and
I<min-closure-src>.

=head1 BUGS

See L<http://bugs.debian.org/botch>.

=head1 SEE ALSO

Debian doc-base Manual F</usr/share/doc/botch/wiki/Home.html>

=head1 AUTHOR

This man page was written by Johannes Schauer. Botch is written by Johannes
Schauer and Pietro Abate.

=head1 COPYRIGHT

Copyright 2012-2014 Johannes Schauer, Pietro Abate

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version. A special linking exception to the GNU Lesser General Public
License applies to this library, see the COPYING file for more information.

