=head1 NAME

botch-graph-tred - find the transitive reduction of a graph in GraphML or dot format

=head1 SYNOPSIS

=over

=item B<botch-graph-tred> [options] [I<ingraph>] [I<outgraph>]

=back

=head1 DESCRIPTION

Create a graph with as few edges as possible while maintaining the same
reachability relationships.

Calculating the transitive reduction particularly helps to facilitate rendering
of dense graphs as it removes the clutter of extra edges.

This tool is the does the same as tred(1) from the graphviz suite but for the
GraphML and dot input instead of only for dot input. It is also orders of
magnitude faster (for unknown reasons).

If either (or both) of the positional arguments is/are omitted, then read
GraphML or dot from standard input and/or write GraphML or dot to standard output,
respectively.  Passing a '-' as either argument, explicitly specifies to read
the input from standard input and write the output to standard output,
respectively.

The input graph type will be determined automatically. If the output graph is
given as a filename, then the output type will be GraphML if the filename ends
with .xml and dot if the filename ends with .dot. If the output is done on
standard output then it will happen in the same format as the input graph.

=head1 OPTIONS

=over 4

=item B<-h, --help>

Display help.

=item B<-v, --verbose>

Be verbose.

=back

=head1 BUGS

See L<http://bugs.debian.org/botch>.

=head1 SEE ALSO

Debian doc-base Manual F</usr/share/doc/botch/wiki/Home.html>

=head1 AUTHOR

This man page was written by Johannes Schauer. Botch is written by Johannes
Schauer and Pietro Abate.

=head1 COPYRIGHT

Copyright 2012-2014 Johannes Schauer, Pietro Abate

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version. A special linking exception to the GNU Lesser General Public
License applies to this library, see the COPYING file for more information.

