=head1 NAME

botch-multiarch-interpreter-problem - find all arch:all packages that allow switching architectures

=head1 SYNOPSIS

=over

=item B<botch-multiarch-interpreter-problem> [-h] [-v] graph.xml

=back

=head1 DESCRIPTION

Given a package graph, this program outputs a page in plain text format listing
all Architecture:all and not Multi-Arch:foreign packages that are on a
dependency path between two Architecture:any packages.

More precisely, dose-ceve creates a graph with binary packages and source
packages being the nodes and their dependency and provides relationship being
the edges. This program prints all nodes (or packages) that fulfill the
following criteria:

=over 4

=item the package is not Multi-Arch:foreign

=item the package is Architecture:all

=item the package has a (possibly transitive) predecessor in the dependency graph that is either an Architecture:any binary package or a source package that builds Architecture:any packages

=item the package has a (possibly transitive) successor in the dependency graph that is Architecture:any and not Multi-Arch:foreign. Traversal of the successors stops at Multi-Arch:foreign packages.

=back

=head1 OPTIONS

=over 4

=item B<-v, --verbose>

Be verbose.

=item B<-h, --help>

Print help message.

=item B<--packages>

Packages file to retrieve binary package size

=item B<-H, --html>

Output HTML document with additional statistics

=back

=head1 EXAMPLE

The graph created by dose-ceve must not contains Build-Depends-Indep
dependencies because source packages do not have to be cross-built to create
their Architecture:all packages. These can always be retrieved by native
compilation. The graph does not have to include builds-from relationships
because the Architecture field of source packages is used to determine whether
a source package builds Architecture:any binary packages.

 dose-ceve --deb-drop-b-d-indep --deb-native-arch=amd64 -G pkg -T grml deb://Packages.gz debsrc://Sources.gz > graph.xml
 botch-multiarch-interpreter-problem --packages=Packages.gz graph.xml > out.html
 w3m out.html

=head1 BUGS

See L<http://bugs.debian.org/botch>.

=head1 SEE ALSO

Debian doc-base Manual F</usr/share/doc/botch/wiki/Home.html>

=head1 AUTHOR

This man page was written by Johannes Schauer. Botch is written by Johannes
Schauer and Pietro Abate.

=head1 COPYRIGHT

Copyright 2012-2014 Johannes Schauer, Pietro Abate

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version. A special linking exception to the GNU Lesser General Public
License applies to this library, see the COPYING file for more information.

